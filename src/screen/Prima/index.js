import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class Prima extends Component {
  constructor() {
    super()
    this.state = {
      data: []
    }
  }

  componentDidMount() {
    const { data } = this.state
    let ruang = []
    for (let i = 0; i <= 20; i++) {
      let flag = 0;
      
      
      for (let j = 2; j < i; j++) {
        if (i % j == 0) {
          flag = 1;
          break;
        }
      }

      if (i > 1 && flag == 0) {
        console.log(i);
        ruang.push(i)
      }
      this.setState({ data: ruang })
    }
  }

  render() {
    const { data } = this.state
    console.log(data)
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ fontWeight: 'bold', fontSize: 24 }}>{data}</Text>
      </View>
    )
  }
}
