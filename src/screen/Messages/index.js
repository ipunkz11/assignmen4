import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Image, ScrollView } from 'react-native'
import AsyncStorageLib from '@react-native-async-storage/async-storage'
import messaging from '@react-native-firebase/messaging';
import CButton from '../../components/atoms/CButton';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'

export default class Messages extends Component {
  constructor() {
    super()
    this.state = {
      data: []
    }
  }

  componentDidMount() {
    this._getData();

    messaging()
      .getToken()
      .then(Token => {
        console.log(Token)
      })
  }

  _getData = async () => {
    try {
      const jsonValue = await AsyncStorageLib.getItem('newInboxData')
      console.log(jsonValue)
      return (
        jsonValue != null && this.setState({ data: JSON.parse(jsonValue) })
      )
    } catch (e) {
      console.log(e)
    }
  }

  _deleteData = async () => {
    await AsyncStorageLib.removeItem('newInboxData')
    this.setState({ data: [] })
  }

  render() {
    const { data } = this.state
    const { navigation } = this.props
    return (
      <View style={styles.head}>
        <View style={styles.icon}>
          <Icon name="keyboard-backspace" size={40} color="#900" onPress={() => navigation.goBack()} />
          <Text style={{ fontWeight: 'bold', color: 'black' }}>MESSAGES</Text>
          <Icon name="trash-can-outline" size={40} color="#900" onPress={() => this._deleteData()} />
        </View>

        <ScrollView style={{ marginBottom: 50 }}>
          {data.length > 0 && data.map((v, i) => {
            return (
              <View key={i} style={styles.wrap}>
                <View>
                  <TouchableOpacity onPress={() => {
                    navigation.navigate('DetailMessages', {
                      message: {
                        title: v.title,
                        body: v.body,
                        image: v.image,
                      },
                    })
                  }}>
                    <Image source={{ uri: v.image }} style={styles.image} />
                  </TouchableOpacity>
                </View>
                <View style={styles.wrapText}>
                  <Text style={{ fontWeight: 'bold' }}>{v.title}</Text>
                  <Text>{v.body}</Text>
                </View>
              </View>
            )
          })}
        </ScrollView>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  head: {
    paddingHorizontal: 10,
    backgroundColor: 'orange'
  },
  icon: {
    marginVertical:5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  wrap: {
    flexDirection: 'row',
    borderRadius: 10,
    borderWidth: 1,
    marginVertical: 5,
    backgroundColor: 'aqua',
  },
  wrapText: {
    justifyContent: 'space-evenly',
    paddingHorizontal: 10,
  },
  image: {
    width: 80,
    height: 80,
    borderRadius: 10,
    borderColor: 'green',
    borderWidth: 2,
  }
})
