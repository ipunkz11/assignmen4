import React, { Component } from 'react'
import { Text, View } from 'react-native'
import firestore from '@react-native-firebase/firestore';

export default class Firebase extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    firestore()
      .collection('Users')
      .doc('Pengguna')
      .onSnapshot(res => {
        this.setState({ data: [res.data()] });
        console.log(res)
      });
  }

  render() {
    const { data } = this.state
    console.log(data)
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
        {data && data.map((v, i) => {
          return (
            <View key={i}>
              <Text>{v.email}</Text>
            </View>
          )
        })}
      </View>
    )
  }
}