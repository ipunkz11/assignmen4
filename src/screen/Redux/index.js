import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput, Button, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'

export class Redux extends Component {
    constructor(){
        super()
        this.state = {
            data:[],
            name: '',
            address: '',
            onSelect: false,
            onEdit: false,
            checked:true,
        };
    }


    render() {
        return (
            <View>
                <Text style={{ textAlign: 'center', marginVertical: 10 }}>CRUD in Redux</Text>
                <View style={styles.card}>
                    <View style={styles.tableHeader}>
                        <Text style={styles.number}>ID</Text>
                        <Text style={styles.title}>Name</Text>
                        <Text style={styles.title}>Address</Text>
                        <Text style={{ width: '20%', textAlign: 'center' }}>Action</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        students: state.students,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        add: data => {
            dispatch({
                type: 'ADD-STUDENT',
                payload: data,
            });
        },

        delete: data => {
            dispatch({
                type: 'DELETE-STUDENT',
                payload: data,
            });
        },

        updateData: v => {
            dispatch({
                type: 'UPDATE-DATA',
                payload: v,
            });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Redux);

const styles = StyleSheet.create({
    card: {
        padding:10,
        borderRadius:20,
        borderWidth:1,
        margin:5
    },
    tableHeader:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
    },
    number:{
        width:'10%',
        alignItems:'center',
    },
    title:{
        width:'30%',
    },
})