import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class Fibonacci extends Component {
  constructor() {
    super()
    this.state = {
      data: []
    }
  }

  componentDidMount() {
    const { data } = this.state
    let x = 0,
      y = 0,
      z = 1,
      result = x + y;

    for (let i = 0; i < 20; i++) {
      data.push(result)

      x = y,
        y = z,
        z = result;
      result = y + z;

      if (result > 20) {
        break;
      }
    }
    this.setState({ data: data })
  }



  render() {
    const { data } = this.state
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ fontWeight: 'bold', fontSize: 24 }}>{data}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({})
