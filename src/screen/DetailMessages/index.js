import React, { Component } from 'react'
import { Text, StyleSheet, View, Image, TouchableOpacity } from 'react-native'

export default class DetailMessages extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: []
    }
  }

  render() {
    const { message } = this.props.route.params
    const {navigation} = this.props
    return (
      <View style={styles.head}>
        <View>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image source={{ uri: message.image }} style={styles.image} />
          </TouchableOpacity>
        </View>
        <View style={styles.wrap}>
          <View style={styles.wrapText}>
            <Text style={{ fontWeight: 'bold', textAlign: 'center', fontSize: 20, color: 'black' }}>{message.title}</Text>
            <Text style={{ color: 'red' }}>{message.body}</Text>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  head: {
    flex: 1,
    paddingHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'orange',
  },
  wrap: {
    flexDirection: 'row',
    borderRadius: 10,
    borderWidth: 5,
    marginBottom: 10,
    borderColor: 'aqua',
  },
  wrapText: {
    justifyContent: 'space-evenly',
    paddingHorizontal: 10,
    backgroundColor: 'aqua'
  },
  image: {
    width: 250,
    height: 250,
    borderRadius: 10,
    borderColor: 'green',
    borderWidth: 5,
    borderColor: 'aqua',
    marginBottom: 20,
  }
})
