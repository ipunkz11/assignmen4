import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { Ganjil, Genap, GetPost, GetUser, Anastasia, Redux, Firebase, Fibonacci, Messages, Prima, DetailMessages, Home } from '../screen'

const Stack = createNativeStackNavigator();

const Route = () => {
    const Off = { headerShown: false }
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={Home} options={Off} />
                <Stack.Screen name="Ganjil" component={Ganjil} options={Off} />
                <Stack.Screen name="Genap" component={Genap} options={Off} />
                <Stack.Screen name="GetPost" component={GetPost} options={Off} />
                <Stack.Screen name="GetUser" component={GetUser} options={Off} />
                <Stack.Screen name="Anastasia" component={Anastasia} options={Off} />
                <Stack.Screen name="Redux" component={Redux} options={Off} />
                <Stack.Screen name="Firebase" component={Firebase} options={Off} />
                <Stack.Screen name="Fibonacci" component={Fibonacci} options={Off} />
                <Stack.Screen name="Messages" component={Messages} options={Off} />
                <Stack.Screen name="Prima" component={Prima} options={Off} />
                <Stack.Screen name="DetailMessages" component={DetailMessages} options={Off} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default Route;
