const initialState = {
    students: [
        { id: 1, name: 'Messi', address: 'Sleman' },
        { id: 2, name: 'C. Ronaldo', address: 'Bandung' },
        { id: 1, name: 'Kaka', address: 'Ngawi' },
        { id: 1, name: 'Ronaldinho', address: 'Brebes' },
        { id: 1, name: 'Tevez', address: 'Cihaseum' },
    ],
    currentUser: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD-STUDENT':
            return {
                ...state,
                students: [...state.students, action.payload],
            };
        case 'DELETE-STUDENT':
            return {
                ...state,
                students: state.students.filter(value => {
                    return value.id != action.payload;
                }),
            };
        // case 'LOGIN':
        //     return {
        //         ...state,
        //         currentUser: action.payload,
        //     };
        case 'UPDATE-DATA':
            return {
                ...state,
                students: action.payload,
            };
        default:
            return state
    }
}

export default reducer